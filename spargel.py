# -*- coding: utf-8 -*-

from __future__ import print_function

def main():
    Gericht.personen(2)

    zwiebel = Zwiebel()
    zwiebelstuecke = zwiebel.schneiden(StueckGroesse.KLEIN)   

    spargel = GruenerSpargel().waschen()
    spargelstuecke = spargel.schneiden(StueckGroesse.ZWEI_CM)
    spargelstuecke = [stueck for stueck in spargelstuecke if not (stueck.zuDick() or stueck.zuHart())]

    sesamkoerner = [Sesamkorn()]

    pfanne = Pfanne()
    pfanne.hinzufuegen(sesamkoerner)

    while pfanne.erhitze(sesamkoerner):
        if any([korn.springtHoch() for korn in sesamkoerner]):
            break
        
    Teller().hinzufuegen(sesamkoerner)

    pfanne.einoelen()

    pfanne.hinzufuegen(zwiebelstuecke)
    pfanne.hinzufuegen(spargelstuecke)

    topf = Topf()
    wasser = topf.koche(Wasser())

    while pfanne.erhitze([zwiebelstuecke, spargelstuecke]):
        if wasser.kocht():
            spaghetti = topf.koche(Spaghetti())
        
        if spaghetti.istAlDente():
            pfanne.hinzufuegen(spaghetti)
            break
        
    pfanne.hinzufuegen(BaerlauchPesto()).einruehren()
    pfanne.hinzufuegen(sesamkoerner).einruehren()
    pfanne.hinzufuegen(Pfeffer())
    
    Gericht.servieren()

def print_multiline(lines, end="\n"):
    lines = lines.splitlines()
    if len(lines) == 0: return
    for line in lines[:-1]:
        print(line.lstrip())
    print(lines[-1].lstrip(), end=end)

class StueckGroesse(object):
    KLEIN = "kleine"
    ZWEI_CM = "2 cm"

class Pfeffer(object):
    def inPfanne(self):
        print("Pfeffern und gut vermischen.")

class Sesamkorn(object):
    def springtHoch(self):
        print("zu springen.", end=" ")
        return True
    
    def inPfanne(self):
        print_multiline("""Die Sesamkörner
        hinzugeben.""", end=" ")
    
    def erhitzen(self):
        print_multiline("""Der Sesam wird nun geröstet, bis einzelne Körner anfangen""")
        
    def getellert(self):
        print("Dann die Sesamkörner aus der Pfanne in einen Teller schütten.")

class Zwiebel(object):
    def schneiden(self, stueckGroesse):
        print_multiline("""Die Zwiebel(n) in %s Stücke schneiden.
        """ % stueckGroesse)
        return [self]
    
    def inPfanne(self):
        print("Zwiebel-", end=" ")
        
    def erhitzen(self):
        pass

class GruenerSpargel(object):
    def waschen(self):
        print("Den Spargel waschen", end =" ")
        return self
    
    def schneiden(self, stueckGroesse):
        print("und anschließend in %s große Stücke schneiden." % stueckGroesse)
        return [self]
    
    def zuDick(self):
        print("Zu dicke oder", end=" ")
        return False
        
    def zuHart(self):
        print_multiline("""zu harte Stücke aussortieren. Dicke Stücke könnten
        potentiell holzig werden.
        """)
        return False
    
    def inPfanne(self):
        print_multiline("""und Spargelstücke hineingeben und
        anbraten.
        """)
    
    def erhitzen(self):
        pass
        
class Spaghetti(object):
    def istAlDente(self):
        print("al dente kochen.")
        return True
    
    def kochen(self):
        print("Die Spaghetti", end=" ")
        
    def inPfanne(self):
        print_multiline("""Die Spaghetti in die Pfanne zum Spargel und den Zwiebeln geben. Die
        Pfanne kann nun auf niedrigerer Stufe betrieben werden.
        """)
 
class Pfanne(object):
    def __init__(self):
        print("Eine große Pfanne (30 cm) auf hoher Stufe aufheizen.", end=" ")
    
    def hinzufuegen(self, items):
        if type(items) is list:
            items[0].inPfanne()
            return self
                
        items.inPfanne()
        return self
            
    def erhitze(self, items):
        if type(items) is list:
            if type(items[0]) is list: return True
            items[0].erhitzen()
        return True
        
    def einoelen(self):
        print_multiline("""
        Die Pfanne nun gut einölen,""", end=" ")
    
    def einruehren(self):
        pass

class BaerlauchPesto(object):
    def inPfanne(self):
        print("Das Bärlauchpesto in die Pfanne geben und vermischen.", end=" ")

class Topf(object):
    def __init__(self):
        print("Einen Topf mit entsprechend viel Wasser für die Spaghetti aufsetzen.", end=" ")
    
    def koche(self, item):
        item.kochen()
        return item

class Wasser(object):
    def kochen(self):
        print_multiline("""Das
        Wasser zum Kochen bringen und salzen.""", end=" ")
        
    def kocht(self):
        return True

class Teller(object):
    def hinzufuegen(self, items):
        if type(items) is list:
            items[0].getellert()

class Gericht(object):
    anzahlPersonen = 2
    
    @staticmethod
    def personen(anzahlPersonen):
        Gericht.anzahlPersonen = anzahlPersonen
        print_multiline("""
        ###########################################################################
        ###                    Bester Spargel von Welt                          ###
        ###                 Rezept für Martin und Tianyi                        ###
        ###########################################################################""")
        
        p = anzahlPersonen
        sesamPp = 1
        spargelPp = 200
        zwiebelPp = 0.5
        pestoPp = 50
        spaghettiPp = 100
        
        print_multiline("""
        Gericht für %d Personen."""
         % Gericht.anzahlPersonen)
         
        print_multiline("""
        - %d EL Sesam (%d pro Person)
        - %d kleine Zwiebel(n) (%.1f pro Person)
        - %dg grüner Spargel (%dg pro Person, etwa ein halber Bund)
        - %dg Bärlauchperso (%dg pro Person)
        - %dg Spaghetti (%dg pro Person)
        - Schwarzer Pfeffer, gemahlen
        """ % (sesamPp * p, sesamPp, zwiebelPp * p, zwiebelPp, spargelPp * p, spargelPp, 
        pestoPp * p, pestoPp, spaghettiPp * p, spaghettiPp))
    
    @staticmethod
    def servieren():
        print_multiline("""
        Gericht servieren. Guten Appetit wünschen Sven und Katharina!""")

if __name__ == "__main__":
    main() 